#!/usr/bin/python

import sys, os, urllib, re

def usage():
	print 'Usage: needlestack.py <Base_URL> [<max depth level>]'
	return

def check_args():
	if (len(sys.argv) != 2 and len(sys.argv) != 3):
		usage()
		sys.exit(2)

class NeedleStack(object):
	def __init__(self, url, max_level=3):
		self.baseurl = sys.argv[1]
		self.graph = {}
		self.index = {}
		self.max_level = int(max_level)
		self.crawl(url, self.max_level)
	
	def lookup(self,keyword):
		pass
		
	def get_graph(self):
		return self.graph
	
	@staticmethod
	def get_page(url):
		try:
			return urllib.urlopen(url).read()
		except:
			return ""
	
	@staticmethod
	def get_all_links(string):
		list = re.findall('<a href="http://.*?"', string)
		for i in range(len(list)):
			list[i] = str(list[i][8:]).translate(None, '"')
		return set(list)
	
	@staticmethod
	def all_words(string):
		list = re.split('<.*?>', string)
		for i in range(len(list)):
			list[i] = re.findall(r'\w+', list[i])
		list = [item for sublist in list for item in sublist]
		return set(list)
	
	def update_graph(self, urls, key, graph):
		if key in graph:
			original_urls = graph[key]
			for item in urls:
				if item not in original_urls:
  					original_urls.append(item)
  			graph[key] = original_urls
  		else:
  			graph[key] = urls
  		return graph

	def update_index(self, words, current_url, index):
		for word in words:
			if word in index:
				if current_url not in index[word.lower()]:
					index[word.lower()].append(current_url)
			else:
				index[word.lower()] = [current_url]
		return index
  		
	def crawl(self, seed, max_level):
		import collections
		urlbucket = collections.deque()
		urlbucket.append(seed)
		while urlbucket:
			current = urlbucket.popleft()
			alllinks = NeedleStack.get_all_links(NeedleStack.get_page(current))
			allwords = NeedleStack.all_words(NeedleStack.get_page(current))
			self.graph = self.update_graph(list(alllinks), current, self.graph)
			self.index = self.update_index(list(allwords), current, self.index)
			if max_level >= 0:
				for item in alllinks:
					if item not in urlbucket:
						urlbucket.append(item)
			max_level -= 1
		
	def compute_ranks(self, graph):
		d = 0.85
		numloops = 10
		ranks = {}
		npages = len(self.graph)
		for page in self.graph:
			ranks[page] = 1.0 / npages
		for i in range(0, numloops):
			newranks = {}
			for page in self.graph:
				newrank = (1 - d) / npages
				for url in self.graph:
					if page in self.graph[url]:
						newrank += d*(ranks[url]/len(self.graph[url]))
				newranks[page] = newrank
			ranks = newranks
		return ranks

	def lookup(self, keyword):
		import operator
		pages = list()
		if keyword.lower() not in self.index:
			print 'Lookup error: Word Not Found!'
			return None
		webpages = self.index[keyword.lower()]
		ranks_sorted = sorted(self.compute_ranks(self.graph).iteritems(),\
			key=operator.itemgetter(1), reverse=True)
		for x, y in ranks_sorted:
			if x in webpages: pages.append(x)
		return pages

# Main driver 	
check_args()
baseurl = sys.argv[1]
if len(sys.argv) > 2:
	crawler = NeedleStack(baseurl, sys.argv[2])
else:
	crawler = NeedleStack(baseurl)

print 'pages:\n', crawler.lookup('pages')
print 'links:\n', crawler.lookup('links')

'''
# Debugging code
print '===> Graph:'
print crawler.get_graph()
print '===> Ranks:'
print crawler.compute_ranks(crawler.get_graph())
'''

'''
Output:
# ./needlestack.py http://freshsources.com/page1.html
pages:
['http://freshsources.com/page5.html', 'http://freshsources.com/page1.html', 'http://freshsources.com/page2.html', 'http://freshsources.com/page3.html', 'http://freshsources.com/page4.html']
links:
['http://freshsources.com/page1.html', 'http://freshsources.com/page2.html']
'''
