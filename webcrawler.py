#!/usr/bin/python

import sys, os, urllib, re

def usage():
	print 'Usage: view.py <file path> <# of lines>'
	return

def check_args():
	if (len(sys.argv) != 2):
		usage()
		sys.exit(2)
		
def get_page(url):
	try:
		return urllib.urlopen(url).read()
	except:
		return ""

def get_all_links(string):
	list = re.findall('<a href="http://.*?"', string)
	for i in range(len(list)):
		list[i] = str(list[i][8:]).translate(None, '"')
	return set(list)
	
def all_words(string):
	list = re.split('<.*?>', string)
	for i in range(len(list)):
		list[i] = re.findall(r'\w+', list[i])
	list = [item for sublist in list for item in sublist]
	return set(list)
	
def update_graph(urls, key, graph):
	if key in graph:
		original_urls = graph[key]
		for item in urls:
			if item not in original_urls:
  				original_urls.append(item)
  		graph[key] = original_urls
  	else:
  		graph[key] = urls
  	return graph

def update_index(words, current_url, index):
	for word in words:
		if word in index:
			if current_url not in index[word.lower()]:
				index[word.lower()].append(current_url)
		else:
			index[word.lower()] = [current_url]
	return index

def crawl(seed, max_level):
	import collections
	global graph
	global index
	urlbucket = collections.deque()
	urlbucket.append(seed)
	while urlbucket:
		current = urlbucket.popleft()
		alllinks = get_all_links(get_page(current))
		allwords = all_words(get_page(current))
		graph = update_graph(list(alllinks), current, graph)
		index = update_index(list(allwords), current, index)
		if max_level > 0:
			for item in alllinks:
				urlbucket.append(item)
		max_level -= 1
		
def lookup(keyword):
	import operator
	global index
	if keyword.lower() not in index:
		print 'Lookup error: Word Not Found!'
		return None
	webpages = index[keyword.lower()]
	ranks_sorted = sorted(compute_ranks(graph).iteritems(),\
		key=operator.itemgetter(1), reverse=True)
	for x, y in ranks_sorted:
		if x in webpages: print x
	
		
def compute_ranks(graph):
	d = 0.85	# probability that surfer will bail
	numloops = 10
	ranks = {}
	npages = len(graph)
	for page in graph:
		ranks[page] = 1.0 / npages
	for i in range(0, numloops):
		newranks = {}
		for page in graph:
			newrank = (1 - d) / npages
			for url in graph:
				if page in graph[url]: # this url links to page
					newrank += d*(ranks[url]/len(graph[url]))
			newranks[page] = newrank
		ranks = newranks
	return ranks

# Driver program to test my functions
baseurl = sys.argv[1]
graph = dict()
index = dict()

crawl(baseurl, 2)
print 'Graph:'
print graph
print 'Index:'
print index
print 'Ranks:'
print compute_ranks(graph)
print 'Lookup:'
lookup('the')

'''
alllinks = get_all_links(get_page(baseurl))
allwords = all_words(get_page(baseurl))
#print allwords

graph = dict()
print 'Graph:'
graph = update_graph(list(alllinks), baseurl, graph)
print graph

index = dict()
print 'Index:'
index = update_index(list(allwords), baseurl, index)
index = update_index(list(allwords), baseurl, index)
print index
'''